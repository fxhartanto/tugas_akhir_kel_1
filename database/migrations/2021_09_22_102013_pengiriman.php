<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pengiriman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengiriman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mobil_id');
            $table->unsignedBigInteger('barang_id');
            $table->unsignedBigInteger('distributor_id');
            $table->foreign('mobil_id')->references('id')->on('mobil');
            $table->foreign('barang_id')->references('id')->on('barang');
            $table->foreign('distributor_id')->references('id')->on('distributor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengiriman');
    }
}
