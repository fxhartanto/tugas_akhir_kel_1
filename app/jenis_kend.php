<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jenis_kend extends Model
{
    protected $table = 'jenis_kend';
    protected $fillable = ['jenis'];

    public function mobil(){
        return $this->hasMany('App\mobil');
    }
}
