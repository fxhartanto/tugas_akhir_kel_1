<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    protected $table = 'barang';
    protected $fillable = ['nama','deskripsi','berat','stock_id'];

    public function stock(){
        return $this->hasMany('App\stock');
    }
}
