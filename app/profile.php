<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['name','alamat','no_hp','bio','user_id'];

    public function User(){
        return $this->belongsTo('App\User');
    }
}
