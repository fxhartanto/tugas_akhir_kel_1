<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jenis_kend;

class jenisController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jenis_kend = jenis_kend::all();
        return view('jenis_kend.index', compact('jenis_kend'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jenis_kend.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'jenis' => 'required',
    	]);
 
        jenis_kend::create([
    		'jenis' => $request->jenis,
    	]);
 
    	return redirect('/jenis')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jenis_kend = jenis_kend::find($id);
        return view('jenis_kend.show', compact('jenis_kend'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jenis_kend = jenis_kend::find($id);
        return view('jenis_kend.edit', compact('jenis_kend'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'jenis' => 'required',
    	]);

        $jenis_kend = jenis_kend::findorfail($id);
 
        $jenis_kend->update([
    		'jenis' => $request->jenis,
    	]);

        return redirect('/jenis')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis_kend = jenis_kend::findorfail($id);
        $jenis_kend->delete();

        return redirect('/jenis')->with('success', 'Task Created Successfully!');
    }
}
