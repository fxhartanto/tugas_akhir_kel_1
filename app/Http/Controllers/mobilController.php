<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jenis_kend;
use App\mobil;


class mobilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mobil = mobil::all();
        return view('mobil.index', compact('mobil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_kend = jenis_kend::all();
        return view('mobil.create', compact('jenis_kend'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'merk' => 'required',
    		'type' => 'required',
    		'plat_no' => 'required',
    		'jenis_kend_id' => 'required',
    	]);
 
        mobil::create([
    		'merk' => $request->merk,
    		'type' => $request->type,
    		'plat_no' => $request->plat_no,
    		'jenis_kend_id' => $request->jenis_kend_id,
    	]);
 
    	return redirect('/mobil')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mobil = mobil::find($id);
        $jenis_kend = jenis_kend::all()->where('jenis_kend_id','=', $mobil->id);
        return view('mobil.show', compact('mobil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mobil = mobil::findorfail($id);
        $jenis_kend = jenis_kend::all();
        return view('mobil.edit', compact('mobil','jenis_kend'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'merk' => 'required',
    		'type' => 'required',
    		'plat_no' => 'required',
    		'jenis_kend_id' => 'required',
    	]);

        $mobil = mobil::findorfail($id);
 
        $mobil->update([
    		'merk' => $request->merk,
    		'type' => $request->type,
    		'plat_no' => $request->plat_no,
    		'jenis_kend_id' => $request->jenis_kend_id,
    	]);

        return redirect('/mobil')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mobil = mobil::findorfail($id);
        $mobil->delete();

        return redirect('/mobil')->with('success', 'Task Created Successfully!');
    }
}
