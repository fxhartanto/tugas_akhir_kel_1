<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\distributor;
use App\cp;
use RealRashid\SweetAlert\Facades\Alert;

class cpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cp = cp::all();
        return view('cp.index', compact('cp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'no_telp' => 'required',
    		'email' => 'required',
    	]);
 
        cp::create([
    		'nama' => $request->nama,
    		'no_telp' => $request->no_telp,
    		'email' => $request->email,
    	]);
 
    	return redirect('/cp')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cp = cp::find($id);
        return view('cp.show', compact('cp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cp = cp::findorfail($id);
        return view('cp.edit', compact('cp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'no_telp' => 'required',
    		'email' => 'required',
    	]);

        $cp = cp::findorfail($id);
 
        $cp->update([
    		'nama' => $request->nama,
    		'no_telp' => $request->no_telp,
    		'email' => $request->email,
    	]);

        return redirect('/cp')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cp = cp::findorfail($id);
        $cp->delete();

        return redirect('/cp')->with('success', 'Task Created Successfully!');
    }
}
