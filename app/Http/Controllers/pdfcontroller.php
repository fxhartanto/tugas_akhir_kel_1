<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mobil;
use App\barang;
use App\distributor;

class pdfcontroller extends Controller
{
    function pdfmobil(){
        $mobil = mobil::all();
        $pdf = \PDF::loadView('pdf.mobil', compact('mobil'));
        return $pdf->download('mobil.pdf');
    }
    
    function pdfbarang(){
        $barang = barang::all();
        $pdf = \PDF::loadView('pdf.barang', compact('barang'));
        return $pdf->download('barang.pdf');
    }
    
    function pdfdistributor(){
        $distributor = distributor::all();
        $pdf = \PDF::loadView('pdf.distributor', compact('distributor'));
        return $pdf->download('distributor.pdf');
    }
}
