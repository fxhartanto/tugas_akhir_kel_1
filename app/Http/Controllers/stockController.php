<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stock;
use App\barang;

class stockController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stock = stock::all();
        return view('stock.index', compact('stock'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $barang = barang::all();
        return view('stock.create', compact('barang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'barang_id' => 'required',
    		'jumlah' => 'required',
    	]);
 
        stock::create([
    		'jumlah' => $request->jumlah,
    		'barang_id' => $request->barang_id,
    	]);
 
    	return redirect('/jumlah')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stock = stock::find($id);
        $barang = barang::all();
        return view('stock.show', compact('stock'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stock = stock::find($id);
        $barang = barang::all();
        return view('stock.edit', compact('stock','barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'jumlah' => 'required',
            'barang_id' => 'required',
    	]);

        $stock = stock::findorfail($id);
 
        $stock->update([
    		'jumlah' => $request->jumlah,
    		'barang_id' => $request->barang_id,
    	]);

        return redirect('/jumlah')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = stock::findorfail($id);
        $stock->delete();

        return redirect('/jumlah')->with('success', 'Task Created Successfully!');
    }
}
