<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\distributor;
use App\cp;

class distributorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distributor = distributor::all();
        return view('distributor.index', compact('distributor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cp = cp::all();
        return view('distributor.create', compact('cp'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'alamat' => 'required',
    		'no_telp' => 'required',
    		'email' => 'required',
    		'cp_id' => 'required',
    	]);
 
        distributor::create([
    		'nama' => $request->nama,
    		'alamat' => $request->alamat,
    		'no_telp' => $request->no_telp,
    		'email' => $request->email,
    		'cp_id' => $request->cp_id,
    	]);
 
    	return redirect('/distributor')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $distributor = distributor::find($id);
        $cp = cp::all()->where('cp_id','=', $distributor->id);
        return view('distributor.show', compact('distributor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distributor = distributor::findorfail($id);
        $cp = cp::all();
        return view('distributor.edit', compact('distributor','cp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'alamat' => 'required',
    		'no_telp' => 'required',
    		'email' => 'required',
    		'cp_id' => 'required',
    	]);

        $distributor = distributor::findorfail($id);
 
        $distributor->update([
    		'nama' => $request->nama,
    		'alamat' => $request->alamat,
    		'no_telp' => $request->no_telp,
    		'email' => $request->email,
    		'cp_id' => $request->cp_id,
    	]);

        return redirect('/distributor')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distributor = distributor::findorfail($id);
        $distributor->delete();

        return redirect('/distributor')->with('success', 'Task Created Successfully!');
    }
}
