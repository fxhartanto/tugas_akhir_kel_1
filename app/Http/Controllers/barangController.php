<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\stock;
use App\barang;


class barangController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = barang::all();
        return view('barang.index', compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jumlah = stock::all();
        return view('barang.create', compact('jumlah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'deskripsi' => 'required',
    		'berat' => 'required',
    	]);
 
        barang::create([
    		'nama' => $request->nama,
    		'deskripsi' => $request->deskripsi,
    		'berat' => $request->berat,
    	]);
 
    	return redirect('/barang')->with('success', 'Task Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = barang::find($id);
        $jumlah = stock::all()->where('stock_id','=', $barang->id);
        return view('barang.show', compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = barang::find($id);
        $jumlah = stock::all();
        return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'nama' => 'required',
    		'deskripsi' => 'required',
    		'berat' => 'required',
    	]);

        $barang = barang::findorfail($id);
 
        $barang->update([
    		'nama' => $request->nama,
    		'deskripsi' => $request->deskripsi,
    		'berat' => $request->berat,
    	]);

        return redirect('/barang')->with('success', 'Task Created Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = barang::findorfail($id);
        $barang->delete();

        return redirect('/barang')->with('success', 'Task Created Successfully!');
    }
}
