<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cp extends Model
{
    
    protected $table = 'cp_dist';
    protected $fillable = ['nama','no_telp','email'];

    public function distributor(){
        return $this->belongsTo('App\distributor');
    }
}
