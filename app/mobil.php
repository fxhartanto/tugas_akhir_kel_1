<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mobil extends Model
{
    protected $table = 'mobil';
    protected $fillable = ['merk','type','plat_no','jenis_kend_id'];

    public function jenis_kend(){
        return $this->belongsTo('App\jenis_kend');
    }
}
