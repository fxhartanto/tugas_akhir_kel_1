<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stock extends Model
{
    protected $table = 'stock';
    protected $fillable = ['barang_id','jumlah'];

    public function barang(){
        return $this->belongsTo('App\barang');
    }
}