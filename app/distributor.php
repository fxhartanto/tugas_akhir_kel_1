<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class distributor extends Model
{
    
    protected $table = 'distributor';
    protected $fillable = ['nama','alamat','no_telp','email','cp_id'];

    public function cp(){
        return $this->belongsTo('App\cp');
    }
}
