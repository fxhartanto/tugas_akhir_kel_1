<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});

Route::group(['middleware' => ['auth']], function(){
    Route::resource('jenis', 'jenisController');
    
    Route::resource('jumlah', 'stockController');
});

Route::resource('distributor', 'distributorController');

Route::resource('cp', 'cpController');

Route::resource('mobil', 'mobilController');

Route::resource('profile', 'profileController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('barang', 'barangController');

Route::get('/mobilpdf','pdfController@pdfmobil');

Route::get('/barangpdf','pdfController@pdfbarang');

Route::get('/distributorpdf','pdfController@pdfdistributor');