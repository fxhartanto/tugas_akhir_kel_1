@extends('master')

@section('judul')
    Tampilkan Barang
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @auth
                <a href="/barang/create" class="btn btn-info my-3">Add Data +</a>
                <a href="/barangpdf" class="btn btn-info my-3">Export To PDF</a>       
                @endauth
                <div class="table-responsive">
                    <table id="multi_col_order"
                        class="table table-striped table-bordered display no-wrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Deskripsi</th>
                                <th>Berat</th>
                                <th>Jumlah</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($barang as $item)
                            <tr>
                                <td>{{$item->nama}}</td>                                
                                <td>{{$item->deskripsi}}</td>                                
                                <td>{{$item->berat}}</td>                                
                                <td>
                                    <ul>
                                        @foreach ($item->stock as $list)
                                            <li>{{$list->jumlah}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <a href="/barang/{{$item->id}}" class="btn btn-info">Show</a>
                                    @auth
                                        <a href="/barang/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/barang/{{$item->id}}" method="POST">
                                        @csrf @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                    @endauth
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Deskripsi</th>
                                <th>Berat</th>
                                <th>Jumlah</th>
                                <th>Properties</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection