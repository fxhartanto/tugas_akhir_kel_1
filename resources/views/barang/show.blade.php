@extends('master')

@section('judul')
    Detail Data Barang
@endsection

@section('content')
<form action="/jumlah" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Jumlah Barang</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="nama">Nama</label>
                                                    <input type="text" class="form-control" name = "nama" id = "nama" value= "{{$barang->nama}}" placeholder="Nama Barang" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="deskripsi">Deskripsi</label>
                                                    <input type="text" class="form-control" name = "deskripsi" id = "deskripsi" value= "{{$barang->deskripsi}}" placeholder="Deskripsi" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="berat">Berat</label>
                                                    <input type="text" class="form-control" name = "berat" id = "berat" value= "{{$barang->berat}}" placeholder="Berat" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection