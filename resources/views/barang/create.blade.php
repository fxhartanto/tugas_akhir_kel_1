@extends('master')

@section('judul')
    Input Data Barang
@endsection

@section('content')
<form action="/barang" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Input Barang</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" name = "nama" id = "nama" placeholder="Masukan Nama Barang">
                                    </div>
                                </div>
                                @error('nama')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="deskripsi">Deskripsi</label>
                                        <input type="text" class="form-control" name = "deskripsi" id = "deskripsi" placeholder="Masukan Deskripsi">
                                    </div>
                                </div>
                                @error('deskripsi')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="berat">Berat</label>
                                        <input type="text" class="form-control" name = "berat" id = "berat" placeholder="Masukan Berat">
                                    </div>
                                </div>
                                @error('berat')
                                    <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                @enderror
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>
 

@endsection