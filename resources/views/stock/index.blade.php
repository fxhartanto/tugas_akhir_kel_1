@extends('master')

@section('judul')
    Tampilkan Jumlah Barang
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <a href="/jumlah/create" class="btn btn-info my-3">Add Data +</a>
                <div class="table-responsive">
                    <table id="multi_col_order"
                        class="table table-striped table-bordered display no-wrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($stock as $item)
                            <tr>
                                <td>{{$item->barang->nama}}</td>
                                <td>{{$item->jumlah}}</td>
                                <td>
                                    <a href="/jumlah/{{$item->id}}" class="btn btn-info">Show</a>
                                    <a href="/jumlah/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/jumlah/{{$item->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Properties</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection