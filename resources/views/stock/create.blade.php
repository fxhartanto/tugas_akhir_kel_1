@extends('master')

@section('judul')
    Buat Jumlah Barang
@endsection

@section('content')
<form action="/jumlah" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tambah Jumlah Barang</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="form-group mb-4">
                                    <label for="barang_id">Nama Barang</label>
                                    <select class="form-control" id="barang_id" name="barang_id">
                                        @foreach ($barang as $item)
                                            <option value="{{$item->id}}">{{$item->nama}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group my-1">
                                        <label for="jumlah">Jumlah Barang</label>
                                        <input type="text" class="form-control" name = "jumlah" id = "jumlah" placeholder="Masukan Jumlah Barang">
                                    </div>
                                </div>
                                @error('jumlah')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>
 

@endsection