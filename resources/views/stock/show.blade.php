@extends('master')

@section('judul')
    Detail Jumlah Barang
@endsection

@section('content')
<form action="/jumlah" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tampilkan Jumlah Barang</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="barang_id">Nama Barang</label>
                                    <input type="text" class="form-control" name = "barang_id" id = "barang_id" value="{{$stock->barang->nama}}"placeholder="Masukan Jumlah Barang" disabled>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group my-1">
                                        <label for="jumlah">Jumlah Barang</label>
                                        <input type="text" class="form-control" name = "jumlah" id = "jumlah" value="{{$stock->jumlah}}" placeholder="Masukan Jumlah Barang" disabled>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection