@extends('master')

@section('judul')
    Detail Jenis Kendaraan
@endsection

@section('content')
<form action="/jenis" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tampilkan Jenis Kendaraan</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="jenis">Jenis Kendaraan</label>
                                        <p>{{$jenis_kend->jenis}}</p>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection