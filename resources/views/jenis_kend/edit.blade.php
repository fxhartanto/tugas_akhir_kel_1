@extends('master')

@section('judul')
    Edit Jenis Kendaraan
@endsection

@section('content')

<form action="/jenis/{{$jenis_kend->id}}" method="post">
    @csrf
    @method('put')
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Jenis Kendaraan</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="jenis">Jenis Kendaraan</label>
                                        <input type="text" class="form-control" name = "jenis" id = "jenis" value ="{{$jenis_kend->jenis}}" placeholder="Masukan Jenis Kendaraan">
                                    </div>
                                </div>
                                @error('jenis')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection