@extends('master')

@section('judul')
    Tampilkan Jenis Kendaraan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <a href="/jenis/create" class="btn btn-info my-3">Add Data +</a>
                <div class="table-responsive">
                    <table id="multi_col_order"
                        class="table table-striped table-bordered display no-wrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Jenis Kendaraan</th>
                                <th>List Mobil</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($jenis_kend as $item)
                            <tr>
                                <td>{{$item->jenis}}</td>
                                <td>
                                    <ul>
                                        @foreach ($item->mobil as $list)
                                            <li>{{$list->merk}} - {{$list->type}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    <a href="/jenis/{{$item->id}}" class="btn btn-info">Show</a>
                                    <a href="/jenis/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/jenis/{{$item->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Jenis Kendaraan</th>
                                <th>List Mobil</th>
                                <th>Properties</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection