@extends('master')

@section('judul')
    Edit Jenis Kendaraan
@endsection

@section('content')
<form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Profile</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="name">Nama</label>
                                                <input type="text" class="form-control" name = "name" id = "name" value= "{{$profile->user->name}}" placeholder="Masukan Nama">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="alamat">Alamat</label>
                                                <textarea class="form-control" name = "alamat" id = "alamat" placeholder="Masukan Alamat">{{$profile->alamat}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="no_hp">Nomor Handphone</label>
                                                <input type="text" class="form-control" name = "no_hp" id = "no_hp" value= "{{$profile->no_hp}}" placeholder="Masukan Nomor Handphone">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="bio">Bio</label>
                                                <textarea class="form-control" name = "bio" id = "bio" placeholder="Masukan Bio">{{$profile->bio}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection