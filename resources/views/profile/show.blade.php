@extends('master')

@section('judul')
    Detail Jenis Kendaraan
@endsection

@section('content')
<form action="/jenis" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Jenis Kendaraan</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="name">Nama</label>
                                                    <input type="text" class="form-control" name = "name" id = "name" value= "{{$profile->name}}" placeholder="Masukan Nama" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="alamat">Alamat</label>
                                                    <textarea class="form-control" name = "alamat" id = "alamat" placeholder="Masukan Alamat" disabled>{{$profile->alamat}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="no_hp">Nomor_Handphone</label>
                                                    <input type="text" class="form-control" name = "no_hp" id = "no_hp" value= "{{$profile->no_hp}}" placeholder="Masukan Nomor Handphone" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="bio">Bio</label>
                                                    <textarea class="form-control" name = "bio" id = "bio" placeholder="Masukan Bio" disabled>{{$profile->bio}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                    <a href="/profile/{{auth::user()->id}}/edit" class="btn btn-info my-3">Edit Data</a>   
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection