@extends('master')

@section('judul')
    Edit List Contact Person
@endsection

@section('content')
<form action="/cp/{{$cp->id}}" method="post">
    @csrf
    @method('put')
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Buat Input Contact Person</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" name = "nama" id = "nama" value="{{$cp->nama}}" placeholder="Masukan Nama Contact Person">
                                    </div>
                                </div>
                                @error('merk')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="no_telp">No Telepon</label>
                                        <input type="text" class="form-control" name = "no_telp" id = "no_telp" value="{{$cp->no_telp}}" placeholder="Masukan Nomor Telepon">
                                    </div>
                                </div>
                                @error('type')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name = "email" id = "email" value="{{$cp->email}}"placeholder="Masukan Email">
                                    </div>
                                </div>
                                @error('email')
                                    <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                @enderror
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection