@extends('master')

@section('judul')
    Tampilkan Contact Person
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @auth
                <a href="/cp/create" class="btn btn-info my-3">Add Data +</a>    
                @endauth
                <div class="table-responsive">
                    <table id="multi_col_order"
                        class="table table-striped table-bordered display no-wrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>nama</th>
                                <th>no hp</th>
                                <th>email</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cp as $item)
                            <tr>
                                <td>{{$item->nama}}</td>                                
                                <td>{{$item->no_telp}}</td>                                
                                <td>{{$item->email}}</td>
                                <td>
                                    <a href="/cp/{{$item->id}}" class="btn btn-info">Show</a>
                                    @auth
                                        <a href="/cp/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/cp/{{$item->id}}" method="POST">
                                        @csrf @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                    @endauth
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>nama</th>
                                <th>no hp</th>
                                <th>email</th>
                                <th>Properties</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection