@extends('master')

@section('judul')
    Buat List Mobil
@endsection

@section('content')
<form action="/mobil" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Buat Input Mobil</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="merk">Merk</label>
                                        <input type="text" class="form-control" name = "merk" id = "merk" placeholder="Masukan Merk Mobil">
                                    </div>
                                </div>
                                @error('merk')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                        <input type="text" class="form-control" name = "type" id = "type" placeholder="Masukan Type Mobil">
                                    </div>
                                </div>
                                @error('type')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="plat_no">Plat Nomor</label>
                                        <input type="text" class="form-control" name = "plat_no" id = "plat_no" onkeyup="this.value = this.value.toUpperCase();" placeholder="Masukan Plat Nomor Mobil">
                                    </div>
                                </div>
                                @error('plat_no')
                                    <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group mb-4">
                                <label for="jenis_kend_id">Jenis Kendaraan</label>
                                <select class="form-control" id="jenis_kend_id" name="jenis_kend_id">
                                    @foreach ($jenis_kend as $item)
                                        <option value="{{$item->id}}">{{$item->jenis}}</option>
                                    @endforeach
                                </select>
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>
 

@endsection