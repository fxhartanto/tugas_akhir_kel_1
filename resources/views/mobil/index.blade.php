@extends('master')

@section('judul')
    Tampilkan Mobil
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @auth
                <a href="/mobil/create" class="btn btn-info my-3">Add Data +</a>
                
                <a href="/mobilpdf" class="btn btn-info my-3">Export To PDF</a>       
                @endauth
                <div class="table-responsive">
                    <table id="multi_col_order"
                        class="table table-striped table-bordered display no-wrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Merk Mobil</th>
                                <th>Type Mobil</th>
                                <th>Plat Nomor Mobil</th>
                                <th>Jenis Kendaraan</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mobil as $item)
                            <tr>
                                <td>{{$item->merk}}</td>                                
                                <td>{{$item->type}}</td>                                
                                <td>{{$item->plat_no}}</td>                                
                                <td>{{$item->jenis_kend->jenis}}</td>
                                <td>
                                    <a href="/mobil/{{$item->id}}" class="btn btn-info">Show</a>
                                    @auth
                                        <a href="/mobil/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/mobil/{{$item->id}}" method="POST">
                                        @csrf @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                    @endauth
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Merk Mobil</th>
                                <th>Type Mobil</th>
                                <th>Plat Nomor Mobil</th>
                                <th>Jenis Kendaraan</th>
                                <th>Properties</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection