@extends('master')

@section('judul')
    Edit Jenis Kendaraan
@endsection

@section('content')
<form action="/distributor/{{$distributor->id}}" method="post">
    @csrf
    @method('put')
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Edit Input distributor</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nama">nama</label>
                                        <input type="text" class="form-control" name = "nama" id = "nama" value="{{$distributor->nama}}" placeholder="Masukan nama distributor">
                                    </div>
                                </div>
                                @error('nama')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" class="form-control" name = "alamat" id = "alamat" value="{{$distributor->alamat}}" placeholder="Masukan Alamat distributor">
                                    </div>
                                </div>
                                @error('alamat')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="no_telp">Nomor Telepon</label>
                                        <input type="text" class="form-control" name = "no_telp" id = "no_telp" value="{{$distributor->no_telp}}" placeholder="Masukan Nomor Telepon distributor">
                                    </div>
                                </div>
                                @error('no_telp')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name = "email" id = "email" value="{{$distributor->email}}" placeholder="Masukan Email distributor">
                                    </div>
                                </div>
                                @error('email')
                                        <div class="alert alert-danger" role="alert">
                                        <strong>Error - </strong> Tabel tidak boleh kosong
                                    </div>
                                    @enderror
                            </div>
                            <div class="form-group mb-4">
                                <label for="cp_id">Contact Person</label>
                                <select class="form-control" id="cp_id" name="cp_id">
                                <option value="{{$distributor->cp_id}}">{{$distributor->cp->nama}}</option>
                                    @foreach ($cp as $item)
                                        <option value="{{$item->id}}">{{$item->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="reset" class="btn btn-dark">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection