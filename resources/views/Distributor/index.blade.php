@extends('master')

@section('judul')
    Tampilkan distributor
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @auth
                <a href="/distributor/create" class="btn btn-info my-3">Add Data +</a>
                
                <a href="/distributorpdf" class="btn btn-info my-3">Export To PDF</a>       
                @endauth
                <div class="table-responsive">
                    <table id="multi_col_order"
                        class="table table-striped table-bordered display no-wrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>nama</th>
                                <th>alamat</th>
                                <th>no hp</th>
                                <th>email</th>
                                <th>contact person</th>
                                <th>Properties</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($distributor as $item)
                            <tr>
                                <td>{{$item->nama}}</td>                                
                                <td>{{$item->alamat}}</td>                                
                                <td>{{$item->no_telp}}</td>
                                <td>{{$item->email}}</td>                                
                                <td>{{$item->cp->nama}}</td>
                                <td>
                                    <a href="/distributor/{{$item->id}}" class="btn btn-info">Show</a>
                                    @auth
                                        <a href="/distributor/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                    <form action="/distributor/{{$item->id}}" method="POST">
                                        @csrf @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                    </form>
                                    @endauth
                                    
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>nama</th>
                                <th>alamat</th>
                                <th>no hp</th>
                                <th>email</th>
                                <th>contact person</th>
                                <th>Properties</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection