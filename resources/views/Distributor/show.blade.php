@extends('master')

@section('judul')
    Detail Jenis Kendaraan
@endsection

@section('content')
<form action="/jenis" method="post">
    @csrf
   <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Jenis Kendaraan</h4>
                    <form action="#">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="merk">Merk</label>
                                                    <input type="text" class="form-control" name = "merk" id = "merk" value= "{{$mobil->merk}}" placeholder="Masukan Merk Mobil" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="type">Type</label>
                                                    <input type="text" class="form-control" name = "type" id = "type" value= "{{$mobil->type}}" placeholder="Masukan Type Mobil" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="plat_no">Plat Nomor</label>
                                                    <input type="text" class="form-control" name = "plat_no" id = "plat_no" value= "{{$mobil->plat_no}}" placeholder="Masukan Plat Nomor Mobil" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="jenis">Jenis Kendaraan</label>
                                                    <input type="text" class="form-control" name = "jenis" id = "jenis" value= "{{$mobil->jenis_kend->jenis}}" placeholder="Masukan Plat Nomor Mobil" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection