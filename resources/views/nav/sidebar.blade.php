<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar" data-sidebarbg="skin6">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class= "list-divider"></li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                    aria-expanded="false"><i class="fas fa-truck"></i><span
                        class="hide-menu">Mobil
                    </span></a>
                    <ul aria-expanded="false" class="collapse first-level base-level-line">
                        <li class="sidebar-item"><a href="/mobil" class="sidebar-link"><span
                                    class="hide-menu"> List Mobil </span></a></li>
                        @auth
                            <li class="sidebar-item"><a href="/jenis" class="sidebar-link"><span
                                    class="hide-menu"> Jenis Kendaraan </span></a></li>
                        @endauth
                        
                    </ul>
                </li>
                <li class= "list-divider"></li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                    aria-expanded="false"><i class="fas fa-industry"></i><span
                        class="hide-menu">Distributor
                    </span></a>
                    <ul aria-expanded="false" class="collapse first-level base-level-line">
                        <li class="sidebar-item"><a href="/distributor" class="sidebar-link"><span
                                    class="hide-menu"> List distributor </span></a></li>
                            @auth
                                <li class="sidebar-item"><a href="/cp" class="sidebar-link"><span
                                    class="hide-menu"> CP Distributor </span></a></li>
                            @endauth
                        
                    </ul>
                </li>
                <li class= "list-divider"></li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow" href="javascript:void(0)"
                    aria-expanded="false"><i class="fas fa-boxes"></i><span
                        class="hide-menu">Barang
                    </span></a>
                    <ul aria-expanded="false" class="collapse first-level base-level-line">
                        <li class="sidebar-item"><a href="/barang" class="sidebar-link"><span
                                    class="hide-menu"> List Barang </span></a></li>
                            @auth
                                
                        <li class="sidebar-item"><a href="/jumlah" class="sidebar-link"><span
                                    class="hide-menu"> Stok </span></a></li>
                            @endauth
                    </ul>
                </li>
                <li class= "list-divider"></li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
