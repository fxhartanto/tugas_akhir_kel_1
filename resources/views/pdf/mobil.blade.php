<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List Mobil</title>

    <style type="text/css">
        @page {
            margin: 0px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .invoice table {
            margin: 15px;
        }

        .invoice h3 {
            margin-left: 15px;
        }
        
        table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
        </style>

</head>
<body>

<div class="invoice">
    <h3>List Kendaraan</h3>
    <table class = "test" width="100%">
        <thead>
            <tr>
                <th>Merk Mobil</th>
                <th>Type Mobil</th>
                <th>Plat Nomor Mobil</th>
                <th>Jenis Kendaraan</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mobil as $item)
            <tr>
                <td>{{$item->merk}}</td>                                
                <td>{{$item->type}}</td>                                
                <td>{{$item->plat_no}}</td>                                
                <td>{{$item->jenis_kend->jenis}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="information" style="position: absolute; bottom: 0;">
    <table width="100%">
        <tr>
            <td align="left" style="width: 50%;">
                &copy; {{ date('Y') }} {{ config('app.url') }} - All rights reserved.
            </td>
            <td align="right" style="width: 50%;">
                Company Slogan
            </td>
        </tr>

    </table>
</div>
</body>
</html>